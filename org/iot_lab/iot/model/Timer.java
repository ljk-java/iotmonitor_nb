package org.iot_lab.iot.model;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * Timer class for count time for last seen info
 * @author leo
 *
 */
public class Timer
{
    private LocalDateTime startDateTime;
    
    public Timer()
    {
	reset();
    }
    
    public void reset()
    {
	startDateTime = LocalDateTime.now();
    }
    
    public String getPastTime()
    {
	LocalDateTime now = LocalDateTime.now();
	LocalDateTime tempDateTime = LocalDateTime.from(startDateTime);
	
	long years = tempDateTime.until(now, ChronoUnit.YEARS);
	tempDateTime = tempDateTime.plusYears(years);
	long month = tempDateTime.until(now, ChronoUnit.MONTHS);
	tempDateTime = tempDateTime.plusMonths(month);
	long days = tempDateTime.until(now, ChronoUnit.DAYS);
	tempDateTime = tempDateTime.plusDays(days);
	long hours = tempDateTime.until(now, ChronoUnit.HOURS);
	tempDateTime = tempDateTime.plusHours(hours);
	long minutes = tempDateTime.until(now, ChronoUnit.MINUTES);
	tempDateTime = tempDateTime.plusMinutes(minutes);
	long seconds = tempDateTime.until(now, ChronoUnit.SECONDS);
		
	return days + "d" + hours + "h" + minutes +"m" + seconds + "s";  
    }

    public long getPastTimeSeconds()
    {
	LocalDateTime now = LocalDateTime.now();
	long seconds = startDateTime.until(now, ChronoUnit.SECONDS);
	return seconds;
    }
    
}
