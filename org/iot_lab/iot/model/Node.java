package org.iot_lab.iot.model;

/**
 * Model of Node
 * @author leo
 *
 */
public class Node
{
    int id;
    String name;
    String location;
    
    String lastSeen;
    int dayCounter;
    Timer timer;
    
    boolean firstSeen = false;
    boolean enable = true;
    
    public Node(int id)
    {
	this.id = id;
	timer = new Timer();
    }
    
    public int getId()
    {
	return id;
    }
    
    public void setName(String name)
    {
	this.name = name;
    }
    
    public String getName()
    {
	return name;
    }

    public void setLocation(String location)
    {
	this.location = location;
    }
    
    public String getLocation()
    {
	return location;
    }
    
    public void disable()
    {
	enable = false;
    }
    
    public void enable()
    {
	enable = true;
    }
    
    public boolean isEnabled()
    {
	return enable;
    }
    
    public String getTimer()
    {
	return timer.getPastTime();
    }
    
    public void resetTimer()
    {
	timer.reset();
	firstSeen = true;
	enable = true;
    }
    
    public long getTimerSeconds()
    {
	return timer.getPastTimeSeconds();
    }
    
    public boolean isFirstSeen()
    {
	return firstSeen;
    }
    
    public int getDayCounter()
    {
	return dayCounter;
    }
    
    public void incrDayCounter()
    {
	dayCounter++;
    }
    
    public void resetDayCounter()
    {
	dayCounter = 0;
    }
}
