package org.iot_lab.iot;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.util.Duration;

import org.iot_lab.iot.model.Node;

public class NodeGui extends Pane
{
    // gui items
    private Label lblName;
    private Label lblLocation;
    private Label lblLastSeen;
    private Label lblDayCounter;
    
    // node
    private Node node;
    
    // timer
    private Timeline timeline;
    
    // default Green color
    final static int GREEN = 223;
    final static int RED = 51;
    final static int BLUE = 51;
  
    // constructor
    public NodeGui(Node node)
    {
	this.node = node;
	

	this.setPrefSize(120, 100);
	
	lblName = new Label(node.getName());
	lblName.setFont(new Font("Courier new", 20));
	lblName.relocate(4, 1);
	
	lblLocation = new Label(node.getLocation());
	lblLocation.relocate(4,  24);
		
	lblLastSeen = new Label(node.getTimer());
	lblLastSeen.relocate(4,  48);
	
	lblDayCounter = new Label("" + node.getDayCounter());
	lblDayCounter.setFont(new Font("Courier new", 32));
	int width = (int) Math.round(lblDayCounter.getWidth());
	lblDayCounter.relocate(96-width, 62);
	
	this.setOnMouseClicked(event -> mouseClicked(event));
	
	// initial color
	if (node.isEnabled()) this.setStyle("-fx-border-color: black; -fx-background-color: #abcdef");
		
	this.getChildren().addAll(lblName, lblLocation, lblLastSeen, lblDayCounter);

	// set timer in timeline for timer task
	timeline = new Timeline(new KeyFrame(Duration.seconds(2), event -> timerTask()));
	timeline.setCycleCount(Animation.INDEFINITE); // loop forever
	timeline.play();
    }
    

    public int getNodeId()
    {
	return node.getId();
    }

    private void mouseClicked(MouseEvent event)
    {
	int clickCount = event.getClickCount();
	
	if (clickCount == 2)
	{
	    node.enable();
	}
	
	if (clickCount > 2)
	{
	    node.disable();
	}
	
	// disabled -> purple
	if (!node.isEnabled()) this.setStyle("-fx-border-color: black; -fx-background-color: #c433ff");

    }
    
    private void timerTask()
    {	
	if (node.isEnabled() && node.isFirstSeen())
	{
	    lblLastSeen.setText(node.getTimer());
	    
	    lblDayCounter.setText("" + node.getDayCounter());
	    int width = (int) Math.round(lblDayCounter.getWidth());
	    lblDayCounter.relocate(116-width, 62);
	    
	    // for setting background color
	    int seconds = (int) node.getTimerSeconds();
	    
	    // RGB
	    // G = 223 -> 
	    // R = 51 -> 223 = geel, daarna G -> 51 = rood
	    // B = 51	blijft 51    
	    int red = RED;
	    int green = GREEN;
	    int blue = BLUE;
	    
	   
	    red = RED + (172 * seconds/300);
	    
	    if (red > 223)
	    {
		red = 223;
	    }
	    
	    if (seconds >= 300)
	    {
		green = GREEN - (172 * (seconds-300)/300);
		if (green < 51)
		{
		    green = 51;
		}
	    }
	    
	    String style = "-fx-border-color: black; -fx-background-color: ";
	    style += "rgb( " + red + ", " + green + ", " + blue + ")";   
	    this.setStyle(style);
	}
    }
    
}
