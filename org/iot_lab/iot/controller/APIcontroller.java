package org.iot_lab.iot.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.iot_lab.iot.model.*;
import org.json.JSONArray;
import org.json.JSONObject;


public class APIcontroller
{
    private static String baseURL = "https://api.iot-lab.org/api/v2/devices";
    
    
    /**
     * Get nodes from API and put them in nodeList
     * @param nodeList
     */
    public void getNodes(ArrayList<Node> nodeList)
    {
	String urlString = baseURL;
	
	try
	{
	    URL url = new URL(urlString);
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");
	    conn.setRequestProperty("Accept", "application/json");
	    
	    if (conn.getResponseCode() != 200)
	    {
		System.out.println("Could not connect to: " + urlString);
	    }
	    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    
	    String rawStream;
	    String jsonString = "";
	    
	    while((rawStream = br.readLine()) != null)
	    {
		jsonString += rawStream;
	    }
	    
	    // Convert to JSON object
	    JSONArray jarray = new JSONArray(jsonString);
	    jarray.forEach(jobject -> {
		JSONObject obj = (JSONObject) jobject;
		// create nodes and add to list
		Node node =  new Node(obj.getInt("id"));
		node.setName(obj.getString("name"));
		node.setLocation(obj.getString("location"));
		nodeList.add(node);
	    });
	    
	    conn.disconnect();
	}
	catch (MalformedURLException ex)
	{
	    ex.printStackTrace();
	}
	catch (IOException ex)
	{
	    ex.printStackTrace();
	}
    }
    
}
